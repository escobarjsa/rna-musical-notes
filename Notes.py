from tkinter import *
import tkinter.font as tkFont
import pyaudio
import wave
import os
import time
import tensorflow as tf
import pandas as pd
import numpy as np
import os

from model import model


class Notes:
    def __init__(self):
        self.App = Tk()
        self.pyaudio = pyaudio.PyAudio()
        self.parameterRecording()
        self.objModel = model()

    def main(self):
        self.model = self.loadModel()
        self.Interface()

        '''Funcion para definir la interfaz grafica'''

    def Interface(self):
        self.App.geometry("400x300")

        self.App.config(bg='SteelBlue4')
        self.App.title("RNA de notas musicales")
        letterStyle = tkFont.Font(family="Lucida Grande", size=15)

        self.labelTitle = Label(self.App, font=("Arial", 12), fg='white', text="Grabar Nota Musical",
                                bg='SteelBlue4', wraplength=100)
        self.labelText = Label(self.App, fg='white', text="", bg='SteelBlue4', wraplength=100)
        self.labelNote = Label(self.App, text="", font=letterStyle, fg="blue")

        self.buttonRecord = Button(self.App, font=("Arial", 12), fg='black', text="Grabar", background='yellow',
                                   command=self.recordNote)
        self.buttonPlay = Button(self.App, font=("Arial", 12), fg='black', text="Reproducir", background='tomato',
                                 command=self.playNote)
        self.buttonPredit = Button(self.App, font=("Arial", 12), fg='black', text="Predicción",
                                   background='DarkGoldenrod2', command=lambda: self.predict(self.model))

        self.labelTitle.place(x=100, y=30, width=200, height=30)
        self.labelNote.place(x=100, y=185, width=200, height=30)
        self.labelText.place(x=100, y=60, width=200, height=30)
        self.buttonRecord.place(x=50, y=100, width=145, height=30)
        self.buttonPlay.place(x=205, y=100, width=145, height=30)
        self.buttonPredit.place(x=125, y=140, width=150, height=30)
        self.App.mainloop()

    '''Funcion para los parametros de audio, sompleos, muestreo, etc'''

    def parameterRecording(self):
        # muestra de sonido, formato samples
        self.Format = pyaudio.paInt16
        # cantidad de canales
        self.Channels = 1
        # Frecuencia de muestreo 44100
        self.Rate = 44100
        self.Chunk = 1024
        # Duración de la muestra de sonido
        self.Time = 2
        self.counter = 0

        '''Función para grabar nota, donde recolectara la información de la grabación y la alojara en la raíz del programa'''

    def recordNote(self):
        self.stream = self.pyaudio.open(format=self.Format,
                                        channels=self.Channels,
                                        rate=self.Rate,
                                        input=True,
                                        frames_per_buffer=self.Chunk)
        self.labelText.config(text="Grabando")
        self.labelText.config(text="Grabación realizada con éxito")
        print("record Note")

        self.Archive = "musical_note.wav"
        # Inicia proceso de grabado
        # Recolectando información del sonido
        frames = []
        for i in range(0, int(self.Rate / self.Chunk * self.Time)):
            data = self.stream.read(self.Chunk)
            frames.append(data)

        # Detener grabación
        self.labelText.config(font=("Arial", 12), fg='white', text="Grabación terminada.",
                              bg='SteelBlue4', wraplength=100)

        self.stream.close()

        # crear y guardar de archivo de audio
        waveFile = wave.open(self.Archive, 'wb')
        waveFile.setnchannels(self.Channels)
        waveFile.setsampwidth(self.pyaudio.get_sample_size(self.Format))
        waveFile.setframerate(self.Rate)
        waveFile.writeframes(b''.join(frames))
        waveFile.close()

    '''Función para cargar el modelo que se encuentra en la carpeta models'''

    def loadModel(self):
        model_path = os.path.join(os.getcwd(), 'models', 'first_model.hdf5')
        model = tf.keras.models.load_model(model_path)
        return model

    '''Funcion para reproducir la nota grabada'''

    def playNote(self):
        print("Reproduciendo nota musical")
        self.labelText.config(text="Reproduciendo nota musical")
        Chunk = 1024

        # Ubucación del Archivo
        f = wave.open(r"" + os.path.join(os.getcwd(), "musical_note.wav"), "rb")

        # iniciar Stream
        stream = self.pyaudio.open(format=self.pyaudio.get_format_from_width(f.getsampwidth()),
                                   channels=f.getnchannels(),
                                   rate=f.getframerate(),
                                   output=True)

        # leer Información
        data = f.readframes(Chunk)

        # reproducir Audio
        while data:
            stream.write(data)
            data = f.readframes(Chunk)

        # detener Stream
        stream.close()

    '''Funcion para predecir la nota musical previamente grabada y que se encuentra en la carpeta raíz'''

    def predict(self, model):
        pathNota = r"" + os.path.join(os.getcwd(), "musical_note.wav")
        vectorData = []

        # Caracteristicas
        data = self.objModel.extractFeatures(pathNota)
        vectorData.append([data])

        # imagem
        feature = pd.DataFrame(vectorData, columns=['data'])
        print(len(feature))

        # convierte características y etiquetas en matrices numpy
        X = np.array(feature.data.tolist())

        result = model.predict(X)
        indicator = 0
        change = result.max()
        for index, outcome in enumerate(result[0]):
            if outcome == change:
                indicator = index
        if (indicator == 0):
            self.labelNote.config(text="Do")
        elif (indicator == 1):
            self.labelNote.config(text="Re")
        elif (indicator == 2):
            self.labelNote.config(text="Fa")
        elif (indicator == 3):
            self.labelNote.config(text="La")
        elif (indicator == 4):
            self.labelNote.config(text="Si")


if __name__ == "__main__":
    notes = Notes()
    notes.main()
