import numpy as np
import pandas as pd
import os
import librosa

# extraccion de caracteristicas
from sklearn.preprocessing import LabelEncoder
from keras.utils import to_categorical
# Division de datos
from sklearn.model_selection import train_test_split

# Modelo
from keras.models import Sequential
from keras.layers import Dense, Dropout, Activation, Flatten
from keras.layers import Convolution2D, MaxPooling2D
from keras.optimizers import Adam
from keras.utils import np_utils
from sklearn import metrics

# Entrenando Modelo
from keras.callbacks import ModelCheckpoint
from datetime import datetime


class model():

    def __init__(self):
        print("Iniciando Model")

    '''Función main'''

    def main(self):
        # cargar data
        data = self.loadData()
        # convertir datos categoricos a numeros
        X, yy = self.convertingData(data)
        # Conjunto de datos de entrenamiento y testeo
        x_train, x_test, y_train, y_test = self.trainingTest(X, yy)
        # Creación y compilación del modelo
        model = self.template(yy, x_test, y_test)
        # Entrenamiento del modelo
        finalModel = self.TrainingModel(model, x_train, x_test, y_train, y_test)
        # Evaluar Modelo
        self.evaluateModel(finalModel, x_test, y_test)

    '''funcion para cargar datos y realizar una clasificación'''

    def loadData(self):
        tag = 0

        features = []
        # directorio actual y se mueve al dataset
        data_folder = os.path.join(os.getcwd(), "dataset")
        # listado de carpetas en el dataset
        folders = os.listdir(data_folder)

        # interación de las carpetas dataset
        for folder in folders:
            # obtener ubucacion dataset, desplazamiento por cada carpeta del dataset
            dir_folder = os.path.join(data_folder, folder)

            # listado de notas musicales
            musicalNotes = os.listdir(dir_folder)

            # Iterar por las notas musicales
            for note in musicalNotes:
                # clasificamos en etiqueta
                if "do" in note:
                    tag = 1
                elif "re" in note:
                    tag = 2
                elif "fa" in note:
                    tag = 3
                elif "la" in note:
                    tag = 4
                elif "si" in note:
                    tag = 5
                else:
                    tag = "no aplica"

                # obtener directorio de la nota musical
                pathNote = os.path.join(dir_folder, note)
                # caracteristicas del audio
                data = self.extractFeatures(pathNote)
                # vector data y etiqueta
                features.append([data, tag])

        # Almacenar la imagen de audio en dataFrame y la etiqueta
        featuresdf = pd.DataFrame(features, columns=['feature', 'etiqueta'])
        print('terminando extracción de las propiedades de ', len(featuresdf), ' filas')
        return featuresdf

    '''separar particulariadades de cada imagen de audio y del espectrograma'''

    def extractFeatures(self, file_name):
        try:
            # funcion mfcc de librosa
            audio, sample_rate = librosa.load(file_name, res_type='kaiser_fast')
            mfccs = librosa.feature.mfcc(y=audio, sr=sample_rate, n_mfcc=40)
            mfccsscaled = np.mean(mfccs.T, axis=0)

        except Exception as e:
            print("Error!, no se encontro la dirección de audio: ", file_name)
            return None

        return mfccsscaled

    '''Funcion para convertir los datos categoricos en numericos'''

    def convertingData(self, featuresdf):
        # convertir a matrices numpy
        X = np.array(featuresdf.feature.tolist())
        y = np.array(featuresdf.etiqueta.tolist())

        # cifrar clasificacipon
        le = LabelEncoder()
        yy = to_categorical(le.fit_transform(y))

        print(X)
        print(y)
        print(yy)

        return X, yy

    '''Funcion para dividir la agrupación de datos en bloques,80 entrenamiento, 20 test, apartar los valores de X y Y'''

    def trainingTest(self, X, yy):
        # separar la agrupación de información
        x_train, x_test, y_train, y_test = train_test_split(X, yy, test_size=0.2, random_state=42)
        # agrupación de información entreno
        print(x_train.shape)
        # agrupacion de información Test
        print(x_test.shape)
        return x_train, x_test, y_train, y_test

    '''Funcion para crear la red neuronal, patron secuencia para cada capa, con keras y tensorflow, prediccion segun 
    la mayor posibilidad '''

    def template(self, yy, x_test, y_test):
        num_labels = yy.shape[1]
        filter_size = 2

        # fabricación del Modelo
        model = Sequential()

        model.add(Dense(256, input_shape=(40,)))
        model.add(Activation('relu'))
        model.add(Dropout(0.5))

        model.add(Dense(256))
        model.add(Activation('relu'))
        model.add(Dropout(0.5))

        model.add(Dense(num_labels))
        model.add(Activation('softmax'))

        model.compile(loss='categorical_crossentropy', metrics=['accuracy'], optimizer='adam')

        model.summary()

        # calculo antes del entrenamiento de la red neuronal
        score = model.evaluate(x_test, y_test, verbose=0)
        accuracy = 100 * score[1]
        print("Pre-training accuracy: %.4f%%" % accuracy)
        return model

    '''Funcion para entrenar el modelo'''

    def TrainingModel(self, model, x_train, x_test, y_train, y_test):
        num_epochs = 100
        num_batch_size = 32

        checkpointer = ModelCheckpoint(filepath='models/first_model.hdf5',
                                       verbose=1, save_best_only=True)
        start = datetime.now()
        model.fit(x_train, y_train, batch_size=num_batch_size, epochs=num_epochs,
                  validation_data=(x_test, y_test), callbacks=[checkpointer], verbose=1)

        duration = datetime.now() - start
        print("Training completed in time: ", duration)
        return model

    ''''Funcion para la evalucación del modelo con los test'''

    def evaluateModel(self, finalModel, x_test, y_test):
        score = finalModel.evaluate(x_test, y_test, verbose=0)
        print("Testing Accuracy: ", score[1])


if __name__ == "__main__":
    newModel = model()
    newModel.main()
